package course.examples.practica_07;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost host = (TabHost) findViewById(R.id.tabHost);
        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Make a phone call:");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Make a phone call:");
        host.addTab(spec);

        //Tab 2
        spec = host.newTabSpec("Send a text message:");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Send a text message:");
        host.addTab(spec);
    }


    public void call(View view) {

        //EditText tv = (EditText)findViewById(R.id.txtPhoneNumber1);
        //String number = tv.getText().toString();
        //Toast.makeText(getBaseContext(),"abc: ",Toast.LENGTH_LONG).show();
        //String number = tv.getText().toString();
        //Uri call = Uri.parse("tel:" + number);
        //Intent surf = new Intent(Intent.ACTION_CALL, call);
        //startActivity(surf);


        try {
            EditText tv = (EditText) findViewById(R.id.txtPhoneNumber1);
            String number = tv.getText().toString();
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("dialing-example", "Call failed", activityException);
        }


    }


    public  void sendMsg(View view)
    {
        EditText tv = (EditText) findViewById(R.id.txtPhoneNumber2);
        String number = tv.getText().toString();

        EditText tv2 = (EditText) findViewById(R.id.txtMessage1);
        String message = tv2.getText().toString();


        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address"  , new String (number));
        smsIntent.putExtra("sms_body"  , message);

        try {
            startActivity(smsIntent);
            finish();
            Log.i("Finished sending SMS...", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this,
                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

}
